package pl.krawczyk.wikisearcher.service;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.mockito.Mockito;
import pl.krawczyk.wikisearcher.exception.WikiClientException;
import pl.krawczyk.wikisearcher.httpclient.WikiHttpClient;
import pl.krawczyk.wikisearcher.model.Query;
import pl.krawczyk.wikisearcher.model.Search;
import pl.krawczyk.wikisearcher.model.WikiSearchResponse;

import java.util.Collections;
import java.util.Set;

public class WikiServiceTest {

    private WikiHttpClient wikiHttpClient;
    private WikiService wikiService = new WikiService();

    @Test
    public void getWikiLinksByFootballClubName() throws WikiClientException {
        // given
        String givenClubName = "Liverpool";

        WikiSearchResponse givenResponse = new WikiSearchResponse();
        Query query = new Query();
        Search search = new Search();
        search.setTitle("Liverpool F.C.");
        query.setSearch(Collections.singletonList(search));
        givenResponse.setQuery(query);

        // when
        wikiHttpClient = Mockito.mock(WikiHttpClient.class);
        Mockito.when(wikiHttpClient.searchClubNameResults(givenClubName)).thenReturn(givenResponse);
        Set<String> urls = wikiService.getWikiLinksByFootballClubName(givenClubName);

        // then
        Assertions.assertThat(urls).contains("https://en.wikipedia.org/wiki/Liverpool_F.C.");
    }
}