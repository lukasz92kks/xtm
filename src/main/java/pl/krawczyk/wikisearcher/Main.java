package pl.krawczyk.wikisearcher;

import pl.krawczyk.wikisearcher.service.WikiService;
import pl.krawczyk.wikisearcher.util.TextUtils;

import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {
        LOG.log(Level.INFO, "Football club name:");
        Scanner scanner = new Scanner(System.in);
        String footballClubName = scanner.nextLine();

        WikiService wikiService = new WikiService();
        Set<String> links = wikiService.getWikiLinksByFootballClubName(footballClubName);

        LOG.log(Level.INFO, "Output links:");
        LOG.log(Level.INFO, "{0}", TextUtils.buildStringFromCollection(links));
    }
}
