package pl.krawczyk.wikisearcher.httpclient.json;

import com.google.gson.Gson;
import pl.krawczyk.wikisearcher.model.WikiSearchResponse;

public class JsonConverter {

    public WikiSearchResponse toWikiSearchResponse(final String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, WikiSearchResponse.class);
    }
}
