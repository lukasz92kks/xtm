package pl.krawczyk.wikisearcher.httpclient;

import pl.krawczyk.wikisearcher.exception.WikiClientException;
import pl.krawczyk.wikisearcher.httpclient.json.JsonConverter;
import pl.krawczyk.wikisearcher.model.WikiSearchResponse;
import pl.krawczyk.wikisearcher.util.TextUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WikiHttpClient {

    private static final String WIKI_SEARCH_REQUEST_TEMPLATE = "https://en.wikipedia.org/w/api.php?action=query&list=search&format=json&srsearch=%22CLUB_NAME%22&srlimit=10";
    private static final int DEFAULT_CONNECTION_TIMEOUT = 5000;    // in milliseconds
    private static final String GET_HTTP_METHOD = "GET";

    private JsonConverter jsonConverter = new JsonConverter();

    public WikiSearchResponse searchClubNameResults(final String footballClubName) throws WikiClientException {
        URL url;
        String response;
        try {
            String request = TextUtils.replaceClubNameAndSpaces(WIKI_SEARCH_REQUEST_TEMPLATE, footballClubName);
            url = new URL(request);
            HttpURLConnection connection = prepareConnection(url);
            response = handleConnection(connection);
        } catch (IOException e) {
            throw new WikiClientException("Wikipedia connection exception", e);
        }

        try {
            return jsonConverter.toWikiSearchResponse(response);
        } catch (Exception e) {
            throw new WikiClientException("Wikipedia parse response exception", e);
        }
    }

    private HttpURLConnection prepareConnection(final URL url) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod(GET_HTTP_METHOD);
        connection.setConnectTimeout(DEFAULT_CONNECTION_TIMEOUT);
        connection.setReadTimeout(DEFAULT_CONNECTION_TIMEOUT);
        return connection;
    }

    private String handleConnection(final HttpURLConnection connection) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder content = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();

        return content.toString();
    }
}
