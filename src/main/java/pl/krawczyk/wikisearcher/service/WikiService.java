package pl.krawczyk.wikisearcher.service;

import pl.krawczyk.wikisearcher.exception.WikiClientException;
import pl.krawczyk.wikisearcher.httpclient.WikiHttpClient;
import pl.krawczyk.wikisearcher.model.Query;
import pl.krawczyk.wikisearcher.model.Search;
import pl.krawczyk.wikisearcher.model.WikiSearchResponse;
import pl.krawczyk.wikisearcher.util.TextUtils;

import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class WikiService {

    private static final Logger LOG = Logger.getLogger(WikiService.class.getName());

    private static final String WIKI_URL = "https://en.wikipedia.org/wiki/TITLE";

    public Set<String> getWikiLinksByFootballClubName(final String footballClubName) {
        WikiHttpClient wikiHttpClient = new WikiHttpClient();
        try {
            WikiSearchResponse wikiSearchResponse = wikiHttpClient.searchClubNameResults(footballClubName);
            List<Search> searches = Optional.ofNullable(wikiSearchResponse)
                    .map(WikiSearchResponse::getQuery)
                    .map(Query::getSearch)
                    .orElse(new ArrayList<>());

            return searches.stream()
                    .map(Search::getTitle)
                    .map(t -> TextUtils.replaceTitleAndSpaces(WIKI_URL, t))
                    .collect(Collectors.toSet());

        } catch (WikiClientException e) {
            LOG.log(Level.SEVERE, "Wikipedia API connection problem occurred: {0}", e);
        }
        return new HashSet<>();
    }
}
