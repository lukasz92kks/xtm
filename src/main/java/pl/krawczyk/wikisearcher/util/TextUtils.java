package pl.krawczyk.wikisearcher.util;

import java.util.Collection;

public class TextUtils {

    private static final String TITLE_TARGET = "TITLE";
    private static final String CLUB_NAME_TARGET = "CLUB_NAME";

    public static String buildStringFromCollection(final Collection<String> collection) {
        StringBuilder sb = new StringBuilder();
        collection.forEach(el -> sb.append(el).append("\n"));
        return sb.toString();
    }

    public static String replaceTitleAndSpaces(final String template, final String title) {
        return replaceTargetAndSpaces(template, TITLE_TARGET, title);
    }

    public static String replaceClubNameAndSpaces(final String template, final String clubName) {
        return replaceTargetAndSpaces(template, CLUB_NAME_TARGET, clubName);
    }

    private static String replaceTargetAndSpaces(final String template, final String target, final String replacement) {
        return template.replace(target, replacement).replaceAll(" ", "_");
    }
}
