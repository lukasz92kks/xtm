package pl.krawczyk.wikisearcher.exception;

public class WikiClientException extends Throwable {

    public WikiClientException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
